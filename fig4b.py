#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from matplotlib.pyplot import figure
import matplotlib


# Setup a plot such that only the bottom spine is shown
def setup(ax):
    ax.spines['right'].set_color('none')
    ax.spines['left'].set_color('none')
    ax.spines['bottom'].set_position('center')
    ax.yaxis.set_major_locator(ticker.NullLocator())
    ax.spines['top'].set_color('none')
    ax.xaxis.set_ticks_position('bottom')
    ax.tick_params(which='major', width=1.00)
    ax.tick_params(which='major', length=15)

    ax.tick_params(which='minor', axis='x', bottom=False, top=False)

    ax.set_xlim(0, 10)
    ax.set_ylim(-1, 1)
    ax.patch.set_alpha(0.0)

x = [2.3, 5.8, 8.2]
x = np.round(x)

y = 0.15 * np.ones(len(x))
s = 120 * np.ones(len(x))
plt.scatter(x, y, s, c="b", marker='v')

font = {
    'size': 14
}

matplotlib.rc('font', **font)

# Multiple Locator
ax = plt.gca()
setup(ax)
ax.xaxis.set_major_locator(ticker.MultipleLocator(1.0))
ax.xaxis.set_minor_locator(ticker.MultipleLocator(0.1))

# Push the top of the top axes outside the figure because we only show the
# bottom spine.
#plt.subplots_adjust(left=0.05, right=0.95, bottom=0.05, top=1.05)

# figsize=(x,y) inches
#figure(num=1, figsize=(64, 4), dpi=80, facecolor='w', edgecolor='k')
#plt.show()

fig = matplotlib.pyplot.gcf()
fig.set_size_inches(9.5, 1.0)  # x, y

img_file = 'fig4b.png'

plt.savefig(img_file, dpi=60, edgecolor='black', bbox_inches="tight", pad_inches=0)  # 50 => 320x240.  75 => 480x360.  62.5 => 400x300.

#im = Image.open(img_file)

#bg = Image.new(im.mode, im.size, im.getpixel((0,0)))
#diff = ImageChops.difference(im, bg)
#diff = ImageChops.add(diff, diff, 2.0, -100)
#bbox = diff.getbbox()
#new_im = im.crop(bbox)
#new_im.save(img_file)

#plt.show()
