#!/usr/bin/env python3

import sys
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image, ImageChops

# Fixing random state for reproducibility
np.random.seed(1)

x = np.random.rand(1, 20)
y = np.random.rand(1, 20)
#s = np.random.rand(*x.shape) * 800 + 500

plt.scatter(x, y, c="blue", marker='x')

xb = [ 0.68 ]
yb = [ 0.61 ]
sb = [ 1000 ]
plt.scatter(xb, yb, sb, c="black", marker=r'$\bullet?$')

dot_x = xb[0] - 0.02   # adjustments because the scatter with label is centered not on the dot
dot_y = yb[0] - 0.005  # adjustments because the scatter with label is centered not on the dot

nn_x = None  # type: float
nn_y = None  # type: float
nn_d = sys.float_info.max
for i in range(len(x[0])):  # brute force linear scan for NN. Only 50 data points.
    d = np.linalg.norm(np.array((x[0][i], y[0][i])) - np.array((dot_x, dot_y)))
    if d < nn_d:
        nn_d = d
        nn_x = x[0][i]
        nn_y = y[0][i]

plt.plot((nn_x, dot_x), (nn_y, dot_y), 'c-')

axes = plt.gca()
r = np.linalg.norm( np.array((dot_x, dot_y)) - np.array((nn_x, nn_y)) )
c = plt.Circle((dot_x, dot_y), r, color='c', fill=False)
axes.add_artist(c)
axes.set_aspect('equal')

plt.axis('off')
plt.box(on=True)

# Get current size
fig_size = plt.rcParams["figure.figsize"]

img_file = 'fig3.png'

plt.savefig(img_file, dpi = 62.5, edgecolor='black', bbox_inches="tight", pad_inches=0)  # 50 => 320x240.  75 => 480x360.  62.5 => 400x300.

im = Image.open(img_file)

bg = Image.new(im.mode, im.size, im.getpixel((0,0)))
diff = ImageChops.difference(im, bg)
diff = ImageChops.add(diff, diff, 2.0, -100)
bbox = diff.getbbox()
new_im = im.crop(bbox)
new_im.save(img_file)

#plt.show()
