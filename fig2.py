#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
from PIL import Image, ImageChops

# Fixing random state for reproducibility
np.random.seed(19680801)

x = [
    -2.5, -2.6, -2.0, -2.0, -0.9, -0.3, -1.0, -0.9, 3.0, 4.0 , 0.9,  0.6, 2   ,  2.1, 4.5, 5.1, 0.22058956, -0.8,
    1.8, -1.5, -1.6, 4.1, 1.2, -2.4, -0.8, -2.4, -1.1, 2, 4.4, 0.3, 2.3
]
y = [
    0.5 , -0.2, 0.2 , -0.8, 2.8 , 2.1 , -1.0, -2.5, 3.0, 0.05,  -1, -2.1, -0.3, -1.9, 0.1, 3.0, 0.5, 0,
    -0.1, 1.1, -1.1, 0.7, -1.2, 0.35, 2.5, -0.5, -1.7, 1.6, 1.5, -1.5, -1.1
]

plt.scatter(x, y, c="blue", marker='x')

grid = False
if grid:
    plt.grid()
else:
    plt.axis('off')
    plt.box(on=False)

axes = plt.gca()

ylim = axes.get_ylim()  # type: tuple
ylim_min = ylim[0]
ylim_max = ylim[1]

xlim = axes.get_xlim()  # type: tuple
xlim_min = xlim[0]
xlim_max = xlim[1]

############################
split1 = 0.22058956
plt.plot((split1, split1), (ylim_min, ylim_max), 'b-')

############################
split2a = 0
plt.plot((xlim_min, split1), (split2a, split2a), 'b-')

split2b = -0.1
plt.plot((split1, xlim_max), (split2b, split2b), 'b-')

############################
split3a = -1.5
plt.plot((split3a, split3a), (split2a, ylim_max), 'b-')

split3b = -1.6
plt.plot((split3b, split3b), (ylim_min, split2a), 'b-')

split3c = 4.1
plt.plot((split3c, split3c), (split2b, ylim_max), 'b-')

split3d = 1.2
plt.plot((split3d, split3d), (ylim_min, split2b), 'b-')

############################
split4a = 0.35
plt.plot((xlim_min, split3a), (split4a, split4a), 'b-')

split4b = 2.5
plt.plot((split3a, split1), (split4b, split4b), 'b-')

split4c = -0.5
plt.plot((xlim_min, split3b), (split4c, split4c), 'b-')

split4d = -1.7
plt.plot((split3b, split1), (split4d, split4d), 'b-')

split4e = 1.6
plt.plot((split1, split3c), (split4e, split4e), 'b-')

split4f = 1.5
plt.plot((split3c, xlim_max), (split4f, split4f), 'b-')

split4g = -1.5
plt.plot((split1, split3d), (split4g, split4g), 'b-')

split4h = -1.1
plt.plot((split3d, xlim_max), (split4h, split4h), 'b-')

############################
qx = [ split1 + 0.15 ]
qy = [ split4e - 0.15 ]

qx2 = [ qx[0] + 0.16 ]  # adjustments because the scatter with label is centered not on the dot
qy2 = [ qy[0] + 0.06 ]  # adjustments because the scatter with label is centered not on the dot
qs = [ 1000 ]
plt.scatter(qx2, qy2, qs, c="black", marker=r'$\bullet?$', zorder=100)  # big zorder to bring to front.

i = 9
plt.plot((qx[0], x[i]), (qy[0], y[i]), 'c-')
r = np.linalg.norm( np.array((qx[0], qy[0])) - np.array((x[i], y[i])) )
# equivalent  r = np.sqrt( (qx[0] - x[i]) ** 2 + (qy[0] - y[i]) ** 2 )
c = plt.Circle((qx[0], qy[0]), r, color='c', fill=False)
axes.add_artist(c)

img_file = 'fig2.png'

plt.savefig(img_file, dpi = 62.5, edgecolor='black', bbox_inches="tight", pad_inches=0)  # 50 => 320x240.  75 => 480x360.  62.5 => 400x300.

im = Image.open(img_file)

bg = Image.new(im.mode, im.size, im.getpixel((0,0)))
diff = ImageChops.difference(im, bg)
diff = ImageChops.add(diff, diff, 2.0, -100)
bbox = diff.getbbox()
new_im = im.crop(bbox)
new_im.save(img_file)

#plt.show()
