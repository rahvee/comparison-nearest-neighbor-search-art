#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np

# Fixing random state for reproducibility
np.random.seed(19680801)

f = 0.4
x = f + np.random.rand(1, 20) * 0.2
y = 1 - f + np.random.rand(1, 20) * 0.2
#s = np.random.rand(*x.shape) * 800 + 500

plt.scatter(x, y, c="blue", marker='x')

x = 1 - f + np.random.rand(1, 20) * 0.2
y = f + np.random.rand(1, 20) * 0.2
#s = np.random.rand(*x.shape) * 800 + 500

plt.scatter(x, y, c="red", marker='o')

x = [ 0.68 ]
y = [ 0.61 ]
s = [ 1000 ]
plt.scatter(x, y, s, c="black", marker=r'$\bullet?$')

plt.axis('off')
plt.box(on=True)

# Get current size
fig_size = plt.rcParams["figure.figsize"]

plt.savefig('fig1.png', dpi = 62.5, edgecolor='black')  # 50 => 320x240.  75 => 480x360.  62.5 => 400x300.

#plt.show()
